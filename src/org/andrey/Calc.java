package org.andrey;


class Calc {

    void showMenuCalc() {
        System.out.println("Вас приветствует канкулятор.\n");
        System.out.println("Выберите необходимую арифметическую операцию: ");
        System.out.println("1.Сложение");
        System.out.println("2.Вычитание");
        System.out.println("3.Умножение");
        System.out.println("4.Деление");
        System.out.println("Введите 'q' для выхода: ");
    }
    boolean isValid(String answer) {

        if (answer == null) {
            return false;
        }

        if (answer.equalsIgnoreCase("q")) {
            return true;
        }

        try {
            final Integer value = Integer.valueOf(answer);

            return value <= 4 && value > 0;

        } catch (NumberFormatException e) {
            System.out.println("Incorrect input... Please, try again.");
            return false;
        }
    }
}
