package org.andrey;

public class Guess {

    public static void main(String args[]) throws java.io.IOException {

        char ch, ignore, answer = 's';

        do {

            System.out.println("Задумана буква из диапозона a-z.");
            System.out.println("Попытайтесь ее угадать: ");

            ch = (char) System.in.read();

            do {
                ignore = (char) System.in.read();
            } while (ignore != '\n');

            if (ch == answer) {
                System.out.println("***Правилно***");
            } else {

                System.out.print("...Извините, нужная буква находится ");

                if (ch < answer) {
                    System.out.println(",ближе к концу алфавита");
                } else {
                    System.out.println("ближе к началу алфавита");
                }

                System.out.println("Повторите попытку! \n");
            }

        } while (answer != ch);
    }
}
