package org.andrey.Vehicle;

 class Vehicle {
    int passengers, fuelcap, mpg;
    Vehicle(int p,int f,int m){
        passengers=p;
        fuelcap=f;
        mpg=m;
    }
    int range(){
        return fuelcap*mpg;
    }
    double fuelneeded (int milles){
        return (double)milles/mpg;
    }
 }
