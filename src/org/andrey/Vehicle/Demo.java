package org.andrey.Vehicle;

public class Demo {
    public static void main(String args[]) {
        Vehicle minivan = new Vehicle(7,16,21);
        int dist=252;

        System.out.println("Для прохождения дистанции в "+dist+" мили," +
                " необхлдимо "+minivan.fuelneeded(dist)+" гллонов топлива");

        System.out.println("Минивэн может провезти "+minivan.passengers+ " пассажиров. " +
                "Дальность поездки "+minivan.range()+" миль");

    }
}

