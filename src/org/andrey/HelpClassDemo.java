package org.andrey;

import java.util.Scanner;

class HelpClassDemo {

    public static void main(String args[]) {

        final Help help = new Help();

        try (final Scanner scanner = new Scanner(System.in)) {
            for (; ; ) {

                String answer;
                do {
                    help.showMenu();

                    answer = scanner.next();
                } while (!help.isValid(answer));

                if (answer.equalsIgnoreCase("q")) {
                    break;
                }

                System.out.println("\n");
                help.helpOn(Integer.valueOf(answer));
            }
        }
    }
}
