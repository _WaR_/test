package org.andrey;

class QueueDemo {

    public static void main(String args[]) {
        Queue bigQ = new Queue(100);
        Queue smallQ = new Queue(4);

        System.out.println("Использование очереди bigQ для сохранения алфавита");

        int i;
        char ch;

        for (i = 0; i < 26; i++)
            bigQ.put((char) ('A' + i));

        System.out.println("Содержимое очереди bigQ: ");

        for (i = 0; i < 26; i++) {
            ch = bigQ.get();
            if (ch != (char) 0) System.out.println(ch);
        }

        System.out.println("\n");
        System.out.println("Использование очереди smallQ для генерации ошибок");

        for (i = 0; i < 5; i++) {
            System.out.println("Попытка сохранения " + (char) ('Z' - i));
            smallQ.put((char) ('Z' - i));
            System.out.println();
        }

        System.out.println("Содержимое smallQ: ");

        for (i = 0; i < 5; i++) {
            ch = smallQ.get();
            if (ch != (char) 0) System.out.print(ch + "\t");
        }
    }
}
