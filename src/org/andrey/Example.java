package org.andrey;


public class Example {

    private static final int CONST_VAR = 1024;

    public static void main(String[] args) {
        System.out.println("peremennaja var1 soderzit " + CONST_VAR);
        System.out.print("peremennaja var2 soderzit var1 / 2 = " + (CONST_VAR / 2));
    }
}