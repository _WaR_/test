package org.andrey;

class Help {

    void helpOn(int answer) {
        switch (answer) {
            case 1:
                System.out.println("Оператор If:\n");
                System.out.println("If (условие) оператор;");
                System.out.println("else оператор;");
                break;
            case 2:
                System.out.println("Оператор switch:\n");
                System.out.println("switch (выражение) {");
                System.out.println("   case константа:");
                System.out.println("   последовательность операторов");
                System.out.println("   break");
                System.out.println(" //...");
                System.out.println("}");
                break;
            case 3:
                System.out.println("Оператор for:\n");
                System.out.println("for(инициализация; условие; итерация);");
                System.out.println("  оператор;");
                break;
            case 4:
                System.out.println("Оператор while:\n");
                System.out.println("while(условие) оператор;");
                break;
            case 5:
                System.out.println("Оператор do-while:\n");
                System.out.println("do{");
                System.out.println("оператор");
                System.out.println("}while(условие);");
                break;
            case 6:
                System.out.println("Оператор break:\n");
                System.out.println("break; или break метка;");
                break;
            case 7:
                System.out.println("Оператор continue:\n");
                System.out.println("continue; или continue метка;");
                break;
        }

        System.out.println();
    }

    void showMenu() {
        System.out.println("Справка:");
        System.out.println("1. if");
        System.out.println("2. switch");
        System.out.println("3. for");
        System.out.println("4. while");
        System.out.println("5. do-while");
        System.out.println("6. break");
        System.out.println("7. continue");
        System.out.println("Введите 'q' для выхода: ");
    }

    boolean isValid(String answer) {

        if (answer == null) {
            return false;
        }

        if (answer.equalsIgnoreCase("q")) {
            return true;
        }

        try {
            final Integer value = Integer.valueOf(answer);

            return value <= 7 && value > 0;

        } catch (NumberFormatException e) {
            System.out.println("Incorrect input... Please, try again.");
            return false;
        }
    }
}
