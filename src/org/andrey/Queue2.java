package org.andrey;

class Queue2 {
    int q2[];
    int putloc;
    int getloc;

    Queue2(int size) {
        q2 = new int[size + 1];
        putloc = getloc = 0;
    }

    void put(int kesh) {
        if (putloc == q2.length - 1) {
            System.out.println(" - Queue is full!!!");
            return;
        }
        putloc++;
        q2[putloc] = kesh;
    }

    int get(){
        if (getloc==putloc) {
            System.out.println(" - Queue is empty!!!");
            return 0;
        }
        getloc++;
        return q2[getloc];
    }
}
