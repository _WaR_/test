package org.andrey;

class QDemo {

    public static void main(String args[]) {
        Queue2 bigQ = new Queue2(100);
        Queue2 smallQ = new Queue2(4);

        System.out.println("Использование очереди bigQ для сохранения алфавита");

        int i;
        int kesh;

        for (i = 0; i < 35; i++)
            bigQ.put(1 + i);

        System.out.println("Содержимое очереди bigQ: ");

        for (i = 0; i < 35; i++) {
            kesh = bigQ.get();
            if (kesh != 0) System.out.print(kesh + " ");
        }

        System.out.println("\n");
        System.out.println("Использование очереди smallQ для генерации ошибок");

        for (i = 0; i < 5; i++) {
            System.out.print("Попытка сохранения " + (5 - i));
            smallQ.put((5 - i));
            System.out.println();
        }

        System.out.println("Содержимое smallQ: ");

        for (i = 0; i < 5; i++) {
            kesh = smallQ.get();
            if (kesh != (char) 0) System.out.print(kesh + " ");
        }
    }
}
